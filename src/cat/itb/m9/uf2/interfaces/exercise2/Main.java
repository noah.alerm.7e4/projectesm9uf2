package cat.itb.m9.uf2.interfaces.exercise2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    //INTERFACE
    /**
     * Aquesta interfície serveix per comprovar els valors dels atributs d'un objecte de classe Camisa.
     */
    @FunctionalInterface
    interface CamisaSeleccionable {
        boolean test(Camisa camisa);
    }

    //MÈTODES
    /**
     * Aquest mètode s'utilitza per filtrar una llista d'objectes de classe Camisa.
     * @param llista Llista de tipus Camisa
     * @param filtre Interfície de seleccionable de camises
     */
    private static ArrayList<Camisa> filtrarCamises(List<Camisa> llista, CamisaSeleccionable filtre) {
        //LLISTA
        ArrayList<Camisa> camises = new ArrayList<>();

        //FILTRE
        for (Camisa c : llista) {
            if (filtre.test(c))
                camises.add(c);
        }

        return camises;
    }

    //MAIN
    public static void main(String[] args) {
        //CAMISES
        Camisa camisa1 = new Camisa(null, "XL", "Vermell");
        Camisa camisa2 = new Camisa(null, "S", "Vermell");
        Camisa camisa3 = new Camisa(null, "M", "Blau");
        Camisa camisa4 = new Camisa(null, "XL", "Blau");
        Camisa camisa5 = new Camisa(null, "M", "Blau");

        //LLISTA
        ArrayList<Camisa> llista = new ArrayList<>(Arrays.asList(camisa1, camisa2, camisa3, camisa4, camisa5));

        System.out.println("\u001B[35mLlista original: \u001B[0m");
        llista.forEach(System.out::println);

        //SUBLLISTES
        System.out.println("\n\u001B[34mCamises de talla XL: \u001B[0m");
        filtrarCamises(llista, (Camisa c) -> c.getTalla().equals("XL")).forEach(System.out::println);

        System.out.println("\n\u001B[34mCamises de color Vermell: \u001B[0m");
        filtrarCamises(llista, (Camisa c) -> c.getColor().equals("Vermell")).forEach(System.out::println);

        System.out.println("\n\u001B[34mCamises de talla M i color Blau: \u001B[0m");
        filtrarCamises(llista, (Camisa c) -> c.getColor().equals("Blau") && c.getTalla().equals("M")).forEach(System.out::println);
    }
}
