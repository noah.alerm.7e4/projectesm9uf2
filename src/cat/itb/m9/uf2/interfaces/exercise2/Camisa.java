package cat.itb.m9.uf2.interfaces.exercise2;

public class Camisa {
    //ATRIBUTS
    private String model;
    private String talla;
    private String color;

    //CONSTRUCTOR
    /**
     * Constructor de Camisa
     * @param model Model de la camisa
     * @param talla Talla de la camisa (ex. XL, S, M...)
     * @param color Color de la camisa (ex. Vermell, Blau...)
     */
    public Camisa(String model, String talla, String color) {
        this.model = model;
        this.talla = talla;
        this.color = color;
    }

    //GETTERS
    public String getTalla() {
        return talla;
    }
    public String getColor() {
        return color;
    }

    //ToSTRING
    @Override
    public String toString() {
        return "Camisa{" +
                "model='" + model + '\'' +
                ", talla='" + talla + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
