package cat.itb.m9.uf2.basics;

public class Person implements Runnable {
    //ATTRIBUTES
    private String name;

    //CONSTRUCTOR
    public Person(String name) {
        this.name = name;
    }

    //RUN
    @Override
    public void run() {
        count();
    }

    //METHODS
    public void count() {
        System.out.printf("\n%s is counting at %s, whose ID is %s\n", name, Thread.currentThread().getName(),
                Thread.currentThread().getId());
        for (int i = 0; i < 20; i++) {
            System.out.println(name + " " + i);
        }
    }
}
