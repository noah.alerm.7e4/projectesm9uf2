package cat.itb.m9.uf2.basics;

public class Example1 {
    //MAIN
    public static void main(String[] args) {
        Thread p1 = new Thread(new Person("Alba"));
        Thread p2 = new Thread(new Person("Clara"));

        //p1.count();
        //p2.count();

        p1.start();
        p2.start();
    }
}
