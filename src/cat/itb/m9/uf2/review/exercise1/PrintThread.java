package cat.itb.m9.uf2.review.exercise1;

public class PrintThread implements Runnable {
    //ATTRIBUTES
    private String sentence;

    //CONSTRUCTOR
    /**
     * PrintThread Constructor
     * @param sentence Sentence to be printed
     */
    public PrintThread(String sentence) {
        this.sentence = sentence;
    }

    //RUN
    @Override
    public void run() {
        printWords(sentence);
    }

    //METHOD
    /**
     * This method is used to print each word at a time.
     * @param sentence Sentence to be printed
     */
    private static synchronized void printWords(String sentence) {
        String[] words = sentence.split(" ");

        for (String word : words) {
            System.out.print(word + " ");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println();
    }

    //MAIN
    public static void main(String[] args) {
        //THREADS
        Thread t1 = new Thread(new PrintThread("Una vegada hi havia un gat"));
        Thread t2 = new Thread(new PrintThread("Once upon a time in the west"));
        Thread t3 = new Thread(new PrintThread("En un lugar de la Mancha"));

        //STARTS
        t1.start();
        t2.start();
        t3.start();
    }
}
