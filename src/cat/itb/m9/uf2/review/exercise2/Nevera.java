package cat.itb.m9.uf2.review.exercise2;

import java.util.concurrent.ThreadLocalRandom;

public class Nevera {
    //ATRIBUTS
    private int cerveses;

    //CONSTRUCTOR
    /**
     * Constructor de Nevera
     * @param cerveses Número de cerveses a la nevera
     */
    public Nevera(int cerveses) {
        this.cerveses = cerveses;
    }

    //METHODS
    /**
     * Aquest mètode s'utilitza per afegir cerveses a la nevera.
     */
    public synchronized void afegeixCervesa() {
        //RANDOM
        ThreadLocalRandom random = ThreadLocalRandom.current();

        int numCervesesAfegides = random.nextInt(1, 7);

        //UPDATE
        cerveses += numCervesesAfegides;

        //OUTPUT
        System.out.printf("%s ha afegit %d cerveses a la nevera.\n", Thread.currentThread().getName(), numCervesesAfegides);
        System.out.printf("Hi ha %d cerveses a la nevera.\n\n", cerveses);
    }

    /**
     * Aquest mètode s'utilitza per treure cerveses de la nevera.
     */
    public synchronized void beuCervesa() {
        //RANDOM
        ThreadLocalRandom random = ThreadLocalRandom.current();

        int numCervesesBegudes = random.nextInt(1, 7);

        //SUFICIENTS
        if (numCervesesBegudes <= cerveses) {
            //UPDATE
            cerveses -= numCervesesBegudes;

            //OUTPUT
            System.out.printf("%s ha begut %d cerveses de la nevera.\n", Thread.currentThread().getName(), numCervesesBegudes);

            if (cerveses == 0)
                System.out.println("S'han acabat les cerveses.");
            else
                System.out.printf("Queden %d cerveses a la nevera.\n", cerveses);
        }
        //NO SUFICIENTS
        else {
            System.out.printf("%s volia beure %d cerveses.\n", Thread.currentThread().getName(), numCervesesBegudes);
            System.out.println("No queden suficients cerveses.");

            if (cerveses > 0)
                System.out.printf("Queden %d cerveses a la nevera.\n", cerveses);
        }

        System.out.println();
    }

    //MAIN
    public static void main(String[] args) {
        //NEVERA
        Nevera nevera = new Nevera(4);

        //THREADS
        Thread t1 = new Thread(nevera::afegeixCervesa, "Pepi");
        Thread t2 = new Thread(nevera::beuCervesa,"Luci");
        Thread t3 = new Thread(nevera::beuCervesa,"Bom");
        Thread t4 = new Thread(nevera::beuCervesa,"Anna");

        //STARTS
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
