package cat.itb.m9.uf2.randoms;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

public class Nevera {
    //ATRIBUTS
    private String marca;
    private int quantitatOus;
    private double preu;
    private boolean rebaixada;

    //CONSTRUCTOR
    /**
     * Constructor de Nevera
     * @param marca Marca de la nevera
     * @param quantitatOus Quantitat d'ous dins la nevera
     * @param preu Preu de la nevera
     * @param rebaixada Està rebaixada?
     */
    public Nevera(String marca, int quantitatOus, double preu, boolean rebaixada) {
        this.marca = marca;
        this.quantitatOus = quantitatOus;
        this.preu = preu;
        this.rebaixada = rebaixada;
    }

    //ToSTRING
    @Override
    public String toString() {
        return String.format("MARCA: %-10s, QUANTITAT D'OUS: %-4d, PREU: %-8.2f, REBAIXADA: %-6s", marca, quantitatOus, preu, rebaixada);
    }


    //MAIN
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);

        //ARRAY DE MARQUES
        String[] marques = {"Beko", "Philips", "Samsung", "LG", "Bosch", "Balay", "Smeg"};

        //THREAD LOCAL RANDOM
        ThreadLocalRandom tlr = ThreadLocalRandom.current();

        //LLISTA
        ArrayList<Nevera> neveres = new ArrayList<>();

        //NEVERES RANDOM
        for (int i = 0; i < 5; i++) {
            //Es fa servir BigDecimal per arrodonir a 2 decimals.
            Nevera nevera = new Nevera(marques[tlr.nextInt(marques.length)], tlr.nextInt(25),
                    BigDecimal.valueOf(tlr.nextDouble(400, 1001)).setScale(2, RoundingMode.HALF_UP)
                            .doubleValue(), tlr.nextBoolean());
            neveres.add(nevera);
        }

        //OUTPUT
        neveres.forEach(System.out::println);
    }
}
