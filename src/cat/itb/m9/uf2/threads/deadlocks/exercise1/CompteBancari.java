package cat.itb.m9.uf2.threads.deadlocks.exercise1;

import java.util.List;

public class CompteBancari {
    //ATRIBUTS
    private int identificador;
    private double saldo;
    private List<Usuari> titulars;

    //CONSTRUCTOR
    /**
     * Constructor de CompteBancari
     * @param identificador Identificador del Compte Bancari
     * @param saldo Saldo del Compte Bancari
     * @param titulars Titulars del Compte Bancari
     */
    public CompteBancari(int identificador, double saldo, List<Usuari> titulars) {
        this.identificador = identificador;
        this.saldo = saldo;
        this.titulars = titulars;
    }
}
