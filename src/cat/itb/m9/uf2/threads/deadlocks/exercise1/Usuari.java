package cat.itb.m9.uf2.threads.deadlocks.exercise1;

import java.util.List;

public class Usuari {
    //ATRIBUTS
    private String nom;
    private String cognoms;
    private List<CompteBancari> comptesBancaris;

    //CONSTRUCTOR
    /**
     * Constructor d'Usuari
     * @param nom Nom de l'Usuari
     * @param cognoms Cognom de l'Usuari
     */
    public Usuari(String nom, String cognoms) {
        this.nom = nom;
        this.cognoms = cognoms;
    }
}
