package cat.itb.m9.uf2.threads.synchronization.exercise1;

public class VendaEntrades {
    //MAIN
    public static void main(String[] args) {
        //RUNNABLE
        Runnable u1 = () -> Taquilla.reservarEntrades("Noah", 1);
        Runnable u2 = () -> Taquilla.reservarEntrades("Joan", 4);
        Runnable u3 = () -> Taquilla.reservarEntrades("Jordi", 2);
        Runnable u4 = () -> Taquilla.reservarEntrades("Pau", 5);
        Runnable u5 = () -> Taquilla.reservarEntrades("Marc", 4);

        //START
        new Thread(u1).start();
        new Thread(u2).start();
        new Thread(u3).start();
        new Thread(u4).start();
        new Thread(u5).start();
    }
}

/**
 * La classe Taquilla serveix per controlar la compra d'entrades.
 */
class Taquilla {
    //ATRIBUTS
    private static int entrades = 10;

    //MÈTODES
    /**
     * Aquest mètode informa cada usuari de l'estat de la seva compra.
     * @param nomUsuari Nom de l'usuari
     * @param numEntrades Número d'entrades a comprar
     */
    public synchronized static void reservarEntrades(String nomUsuari, int numEntrades) {
        System.out.printf("\n%s vol comprar %d entrades.\n", nomUsuari, numEntrades);

        //>4 ENTRADES
        if (numEntrades > 4) {
            System.out.println(nomUsuari + " no pot comprar més de 4 entrades.");
        }
        //<=4 ENTRADES
        else {
            //NO EXHAURIDES
            if (entrades >= numEntrades) {
                System.out.println(nomUsuari + " ha comprat les entrades satisfactòriament.");
                entrades -= numEntrades;
            }
            //EXHAURIDES
            else {
                System.out.println(nomUsuari + " no ha pogut comprar les entrades perquè estan exhaurides.");
            }
        }

        System.out.printf("Queden %d entrades.\n\n", entrades);
    }
}
