package cat.itb.m9.uf2.threads.BlockingQueues.exercici1;

public class UsuariPregunta implements Runnable {
    //ATRIBUTS
    private Xat xat;

    //CONSTRUCTOR
    /**
     * Constructor d'UsuariPregunta
     * @param xat Xat
     */
    public UsuariPregunta(Xat xat) {
        this.xat = xat;
    }

    //RUN
    @Override
    public void run() {
        //PREGUNTES
        String[] preguntes = {"Com estàs?", "Quin temps fa?", "A quina classe vas?", "Quants anys tens?",
                "Quin exercici estàs fent?"};

        //XAT
        for (String pregunta : preguntes) {
            xat.pregunta(pregunta);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        xat.pregunta("No em queden més preguntes.");
    }
}
