package cat.itb.m9.uf2.threads.BlockingQueues.exercici1;

public class Xat {
    //ATRIBUTS
    private boolean resposta = false;

    //MÈTODES
    /**
     * Aquest mètode espera que la pregunta sigui resposta per realitzar una altra pregunta.
     * @param msg Missatge
     */
    public synchronized void pregunta(String msg) {
        while(!resposta){
            try {
                wait();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }

        resposta = false;
        System.out.println("Q: " + msg);
        notify();
    }

    /**
     * Aquest mètode espera que es faci una pregunta per respondre-la.
     * @param msg Missatge
     */
    public synchronized void resposta(String msg) {
        while(resposta){
            try {
                wait();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }

        resposta = true;
        System.out.println("A: " + msg);
        notify();
    }
}
