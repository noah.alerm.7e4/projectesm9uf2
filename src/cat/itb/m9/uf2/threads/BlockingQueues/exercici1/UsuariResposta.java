package cat.itb.m9.uf2.threads.BlockingQueues.exercici1;

public class UsuariResposta implements Runnable {
    //ATRIBUTS
    private Xat xat;

    //CONSTRUCTOR
    /**
     * Constructor d'UsuariResposta
     * @param xat Xat
     */
    public UsuariResposta(Xat xat) {
        this.xat = xat;
    }

    //RUN
    @Override
    public void run() {
        //RESPOSTES
        String[] respostes = {"Bé.", "Fa sol.", "A DAMr.", "Tinc 19 anys.", "El primer."};

        //XAT
        for (String resposta : respostes) {
            xat.resposta(resposta);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        xat.resposta("D'acord.");
    }
}
