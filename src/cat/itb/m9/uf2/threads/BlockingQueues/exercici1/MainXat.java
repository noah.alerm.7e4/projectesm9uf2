package cat.itb.m9.uf2.threads.BlockingQueues.exercici1;

public class MainXat {
    //MAIN
    public static void main(String[] args) {
        //XAT
        Xat xat = new Xat();

        //RUNNABLE
        Runnable u1 = new UsuariPregunta(xat);
        Runnable u2 = new UsuariResposta(xat);

        //THREAD
        Thread t1 = new Thread(u1);
        Thread t2 = new Thread(u2);

        //START
        t1.start();
        t2.start();
    }
}
