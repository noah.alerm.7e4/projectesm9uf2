package cat.itb.m9.uf2.threads.BlockingQueues.exercici3;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Exercici3Main {
    //MAIN
    public static void main(String[] args) {
        //BLOCKING QUEUE
        BlockingQueue<Integer> q = new ArrayBlockingQueue<>(20);

        //EXECUTOR
        ExecutorService executor = Executors.newCachedThreadPool();

        //LECTOR
        executor.submit(new LectorFitxer(q));

        //SUMADOR
        System.out.println(new SumadorFitxer(q).call());
    }
}
