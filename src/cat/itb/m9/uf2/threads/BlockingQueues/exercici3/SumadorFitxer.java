package cat.itb.m9.uf2.threads.BlockingQueues.exercici3;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

public class SumadorFitxer implements Callable<Integer> {
    //ATRIBUTS
    private BlockingQueue<Integer> cua;

    //CONSTRUCTOR
    /**
     * Constructor de SumadorFitxer
     * @param cua BlockingQueue
     */
    public SumadorFitxer(BlockingQueue<Integer> cua) {
        this.cua = cua;
    }

    //CALL
    @Override
    public Integer call() {
        int total = 0;

        //BLOCKING QUEUE
        try {
            while (true) {
                int num = cua.take();

                total += num;
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        return total;
    }
}
