package cat.itb.m9.uf2.threads.BlockingQueues.exercici3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

public class LectorFitxer implements Runnable {
    //ATRIBUTS
    private BlockingQueue<Integer> cua;

    //CONSTRUCTOR
    /**
     * Constructor de LectorFitxer
     * @param cua BlockingQueue
     */
    public LectorFitxer(BlockingQueue<Integer> cua) {
        this.cua = cua;
    }

    //RUN
    @Override
    public void run() {
        //LLISTA DE NOMBRES
        List<Integer> nombres = new ArrayList<>();

        //LECTURA DE FITXERS
        try {
            //SCANNERS
            Scanner sc1 = new Scanner(new File("./integerFiles/file1"));
            Scanner sc2 = new Scanner(new File("./integerFiles/file2"));
            Scanner sc3 = new Scanner(new File("./integerFiles/file3"));

            //ADICIÓ A LA LLISTA
            while (sc1.hasNext()) {
                nombres.add(sc1.nextInt());
                nombres.add(sc2.nextInt());
                nombres.add(sc3.nextInt());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //BLOCKING QUEUE
        try {
            for (int nombre : nombres){
                cua.put(nombre);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
