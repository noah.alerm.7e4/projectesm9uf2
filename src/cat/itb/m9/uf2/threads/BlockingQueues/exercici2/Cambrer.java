package cat.itb.m9.uf2.threads.BlockingQueues.exercici2;

import java.util.concurrent.BlockingQueue;

public class Cambrer implements Runnable {
    //ATRIBUTS
    private BlockingQueue<String> cua;

    //CONSTRUCTOR
    /**
     * Constructor de Cambrer
     * @param cua BlockingQueue
     */
    public Cambrer(BlockingQueue<String> cua) {
        this.cua = cua;
    }

    //RUN
    @Override
    public void run() {
        //BLOCKING QUEUE
        try {
            while (true) {
                Thread.sleep(1000);
                System.out.println(cua.take());
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
