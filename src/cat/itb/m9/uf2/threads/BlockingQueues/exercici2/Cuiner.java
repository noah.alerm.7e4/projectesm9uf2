package cat.itb.m9.uf2.threads.BlockingQueues.exercici2;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

public class Cuiner implements Runnable {
    //ATRIBUTS
    private BlockingQueue<String> cua;

    //CONSTRUCTOR
    /**
     * Constructor de Cuiner
     * @param cua BlockingQueue
     */
    public Cuiner(BlockingQueue<String> cua) {
        this.cua = cua;
    }

    //RUN
    @Override
    public void run() {
        //PLATS
        String[] plats = {"Macarrons", "Espaguetis", "Canelons", "Pollastre a la brasa", "Amanida", "Paella"};

        //RANDOM
        ThreadLocalRandom random = ThreadLocalRandom.current();

        //BLOCKING QUEUE
        try {
            while (true) {
                Thread.sleep(2000);
                cua.put(plats[random.nextInt(plats.length)]);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
