package cat.itb.m9.uf2.threads.BlockingQueues.exercici2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Restaurant {
    //MAIN
    public static void main(String[] args) {
        //BLOCKING QUEUE
        BlockingQueue<String> q = new ArrayBlockingQueue<>(20);

        //EXECUTOR
        ExecutorService executor = Executors.newCachedThreadPool();

        //CUINER
        executor.submit(new Cuiner(q));

        //CAMBRERS
        executor.submit(new Cambrer(q));
        executor.submit(new Cambrer(q));
    }
}
