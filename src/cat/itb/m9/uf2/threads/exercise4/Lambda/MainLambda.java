package cat.itb.m9.uf2.threads.exercise4.Lambda;

import cat.itb.m9.uf2.threads.exercise4.ClasseInternaAnonima.BiciClasseInterna;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainLambda {
    //METHODS
    /**
     * Aquest mètode s'utilitza per comprovar quina és la més ràpida d'una llista de bicis.
     * @param bicis Llista de bicis
     */
    public static void getBiciMesRapida(List<BiciClasseInterna> bicis) {
        BiciClasseInterna biciMesRapida = bicis.get(0);

        for (int i = 1; i < bicis.size(); i++) {
            if (bicis.get(i).getTemps() < biciMesRapida.getTemps())
                biciMesRapida = bicis.get(i);
        }

        System.out.printf("\nLa bici més ràpida és la de %s.\n", biciMesRapida.getNom());
    }

    //MAIN
    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int distancia = 1000;

        //BICIS
        BiciClasseInterna b1 = new BiciClasseInterna("Montse", distancia, inici);
        BiciClasseInterna b2 = new BiciClasseInterna("Fran", distancia, inici);
        BiciClasseInterna b3 = new BiciClasseInterna("Clara", distancia, inici);

        //CLASSES INTERNES ANÒNIMES
        Thread tempsB1 = new Thread(() -> {
            //CÀLCUL DEL TEMPS
            LocalTime acabat = null;

            for (int i = 0; i < b1.getDistancia(); i++) {
                acabat = LocalTime.now();
            }

            b1.setTemps(Duration.between(b1.getInici(), acabat).getNano());
        });
        Thread tempsB2 = new Thread(() -> {
            //CÀLCUL DEL TEMPS
            LocalTime acabat = null;

            for (int i = 0; i < b2.getDistancia(); i++) {
                acabat = LocalTime.now();
            }

            b2.setTemps(Duration.between(b2.getInici(), acabat).getNano());
        });
        Thread tempsB3 = new Thread(() -> {
            //CÀLCUL DEL TEMPS
            LocalTime acabat = null;

            for (int i = 0; i < b3.getDistancia(); i++) {
                acabat = LocalTime.now();
            }

            b3.setTemps(Duration.between(b3.getInici(), acabat).getNano());
        });

        //STARTS
        tempsB1.start();
        try {
            tempsB1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        tempsB2.start();
        try {
            tempsB2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        tempsB3.start();

        //PRINTS
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);

        //BICI MÉS RÀPIDA
        List<BiciClasseInterna> bicis = new ArrayList<>(Arrays.asList(b1, b2, b3));
        getBiciMesRapida(bicis);
    }
}
