package cat.itb.m9.uf2.threads.exercise4.Runnable;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainRunnable {
    //METHODS
    /**
     * Aquest mètode s'utilitza per comprovar quina és la més ràpida d'una llista de bicis.
     * @param bicis Llista de bicis
     */
    public static void getBiciMesRapida(List<BiciRunnable> bicis) {
        BiciRunnable biciMesRapida = bicis.get(0);

        for (int i = 1; i < bicis.size(); i++) {
            if (bicis.get(i).getTemps() < biciMesRapida.getTemps())
                biciMesRapida = bicis.get(i);
        }

        System.out.printf("\nLa bici més ràpida és la de %s.\n", biciMesRapida.getNom());
    }

    //MAIN
    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int distancia = 1000;

        //BICIS
        BiciRunnable b1 = new BiciRunnable("Montse", distancia, inici);
        BiciRunnable b2 = new BiciRunnable("Fran", distancia, inici);
        BiciRunnable b3 = new BiciRunnable("Clara", distancia, inici);

        //RUNS
        b1.run();
        b2.run();
        b3.run();

        //PRINTS
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);

        //BICI MÉS RÀPIDA
        List<BiciRunnable> bicis = new ArrayList<>(Arrays.asList(b1, b2, b3));
        getBiciMesRapida(bicis);
    }
}
