package cat.itb.m9.uf2.threads.exercise4.Runnable;

import java.time.Duration;
import java.time.LocalTime;

public class BiciRunnable implements Runnable {
    //ATRIBUTS
    private String nom;
    private LocalTime inici;
    private int distancia;
    private long temps;

    //CONSTRUCTOR
    /**
     * Constructor de Bici
     * @param nom Nom del propietari
     * @param inici Inici del viatge
     * @param distancia Distància del viatge
     */
    public BiciRunnable(String nom, int distancia, LocalTime inici) {
        this.nom = nom;
        this.distancia = distancia;
        this.inici = inici;
    }

    //GETTERS
    public String getNom() {
        return nom;
    }
    public LocalTime getInici() {
        return inici;
    }
    public int getDistancia() {
        return distancia;
    }
    public long getTemps() {
        return temps;
    }

    //SETTERS
    public void setTemps(long temps) {
        this.temps = temps;
    }

    //RUN
    @Override
    public void run() {
        //CÀLCUL DEL TEMPS
        LocalTime acabat = null;

        for (int i = 0; i < getDistancia(); i++) {
            acabat = LocalTime.now();
        }

        setTemps(Duration.between(getInici(), acabat).getNano());
    }

    //ToSTRING
    @Override
    public String toString() {
        return String.format("%s ha trigat %s unitats de temps en fer una distància de %d.", getNom(), getTemps(),
                getDistancia());
    }
}
