package cat.itb.m9.uf2.threads.exercise4.Thread;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainThread {
    //METHODS
    /**
     * Aquest mètode s'utilitza per comprovar quina és la més ràpida d'una llista de bicis.
     * @param bicis Llista de bicis
     */
    public static void getBiciMesRapida(List<BiciThread> bicis) {
        BiciThread biciMesRapida = bicis.get(0);

        for (int i = 1; i < bicis.size(); i++) {
            if (bicis.get(i).getTemps() < biciMesRapida.getTemps())
                biciMesRapida = bicis.get(i);
        }

        System.out.printf("\nLa bici més ràpida és la de %s.\n", biciMesRapida.getNom());
    }

    //MAIN
    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int distancia = 1000;

        //BICIS
        BiciThread b1 = new BiciThread("Montse", distancia, inici);
        BiciThread b2 = new BiciThread("Fran", distancia, inici);
        BiciThread b3 = new BiciThread("Clara", distancia, inici);

        //STARTS / JOINS
        b1.start();
        try {
            b1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        b2.start();
        try {
            b2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        b3.start();

        //PRINTS
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);

        //BICI MÉS RÀPIDA
        List<BiciThread> bicis = new ArrayList<>(Arrays.asList(b1, b2, b3));
        getBiciMesRapida(bicis);
    }
}
