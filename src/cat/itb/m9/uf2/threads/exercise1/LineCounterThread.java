package cat.itb.m9.uf2.threads.exercise1;

import java.io.File;
import java.util.Scanner;

public class LineCounterThread extends Thread {
    //ATTRIBUTES
    private String fileName;

    //CONSTRUCTOR
    /**
     * LineCounterThread Constructor
     * @param fileName File's path
     */
    public LineCounterThread(String fileName) {
        this.fileName = fileName;
    }

    //RUN
    @Override
    public void run() {
        LineCounter.countLines(new File(fileName));
    }

    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //INPUT
        System.out.print("Enter the file's path (e.g. './fileExamples/Hola.txt'): ");

        LineCounterThread lineCounterThread = new LineCounterThread(sc.nextLine());


        //OUTPUT
        lineCounterThread.start();
    }
}
