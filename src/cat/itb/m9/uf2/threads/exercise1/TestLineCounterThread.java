package cat.itb.m9.uf2.threads.exercise1;

public class TestLineCounterThread {
    //MAIN
    public static void main(String[] args) {
        LineCounterThread counter1 = new LineCounterThread("./fileExamples/Hola.txt");
        LineCounterThread counter2 = new LineCounterThread("./fileExamples/Hello World.txt");
        LineCounterThread counter3 = new LineCounterThread("./fileExamples/Barcelona.txt");

        //OUTPUT
        counter1.start();
        counter2.start();
        counter3.start();
    }
}
