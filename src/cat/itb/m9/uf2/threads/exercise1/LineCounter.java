package cat.itb.m9.uf2.threads.exercise1;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class LineCounter {
    //METHODS
    /**
     * This method is used to count the lines in a file.
     */
    public static void countLines(File fileName) {
        //COUNTER
        int counter = 0;

        try {
            Scanner scanner = new Scanner(fileName);

            //COUNT
            while (scanner.hasNext()) {
                scanner.nextLine();
                counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //OUTPUT
        System.out.printf("There are %d lines in this file.\n", counter);
    }

    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //INPUT
        System.out.print("Enter the file's path (e.g. './fileExamples/Hola.txt'): ");

        //OUTPUT
        countLines(new File(sc.nextLine()));
    }
}
