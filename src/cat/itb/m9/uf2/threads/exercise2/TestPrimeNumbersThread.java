package cat.itb.m9.uf2.threads.exercise2;

public class TestPrimeNumbersThread {
    //MAIN
    public static void main(String[] args) {
        PrimeNumbersThread n1 = new PrimeNumbersThread(8);
        PrimeNumbersThread n2 = new PrimeNumbersThread(54);
        PrimeNumbersThread n3 = new PrimeNumbersThread(98);

        //OUTPUT
        n1.start();
        n2.start();
        n3.start();
    }
}
