package cat.itb.m9.uf2.threads.exercise2;

import java.util.Locale;
import java.util.Scanner;

public class PrimeNumbersRunnable implements Runnable {
    //ATTRIBUTES
    private long firstNum;

    //CONSTRUCTOR
    /**
     * PrimeNumbersThread Constructor
     * @param firstNum First number to check
     */
    public PrimeNumbersRunnable(long firstNum) {
        this.firstNum = firstNum;
    }

    //RUN
    @Override
    public void run() {
        long num = firstNum;

        //PRIME CHECK
        while (!PrimeNumbers.isPrime(num)) {
            num++;
        }

        //OUTPUT
        System.out.printf("The closest next prime number to %d is %d.\n", firstNum, num);
    }

    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in).useLocale(Locale.US);

        //INPUT
        System.out.print("Enter a number: ");

        PrimeNumbersThread prime = new PrimeNumbersThread(sc.nextLong());

        //OUTPUT
        prime.start();
    }
}
