package cat.itb.m9.uf2.threads.exercise2;

import java.util.Locale;
import java.util.Scanner;

public class PrimeNumbers {
    //METHODS
    /**
     * This method checks whether a given number is a prime number or not.
     * @param num given number (LONG)
     * @return true or false
     */
    public static boolean isPrime(long num) {
        //BOOLEAN
        boolean isPrime = true;

        //NEGATIVE NUMBERS / 0 / 1
        if (num <= 1)
            isPrime = false;
        //POSITIVE NUMBERS
        else {
            for (int i = 2; i <= num/2; i++) {
                if (num % i == 0) {
                    isPrime = false;
                    break;
                }
            }
        }

        //OUTPUT
        return isPrime;
    }

    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in).useLocale(Locale.US);

        //INPUT
        System.out.print("Enter a number: ");
        long num = sc.nextLong();

        //OUTPUT
        System.out.printf("%d is a prime number --> %s\n", num, isPrime(num));
    }
}
