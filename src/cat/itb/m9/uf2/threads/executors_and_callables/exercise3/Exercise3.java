package cat.itb.m9.uf2.threads.executors_and_callables.exercise3;

import java.io.File;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.*;

public class Exercise3 {
    //MAIN
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //INPUT
        System.out.print("Enter the directory's path: ");
        String path = scanner.nextLine();

        //DIRECTORY
        File directory = new File(path);

        //TOTAL SIZE
        Long totalSize = 0L;

        //EXECUTOR OPEN
        ExecutorService ex = Executors.newFixedThreadPool(Objects.requireNonNull(directory.listFiles()).length);

        //READING
        int i = 0;
        System.out.println("\n\u001B[35mFILES SIZES:\u001B[0m");
        for (File file : Objects.requireNonNull(directory.listFiles())) {
            //CALLABLE
            DirectoryReader d = new DirectoryReader(file);

            //EXECUTOR SUBMIT
            Future<Long> f = ex.submit(d);

            try {
                //RESULT GETTER
                Long fileSize = f.get();

                //TOTAL UPDATE
                totalSize += fileSize;

                //FILE OUTPUT
                System.out.printf("File %d: %dKB\n", i, fileSize);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            i++;
        }

        //EXECUTOR SHUTDOWN
        ex.shutdown();

        //OUTPUT
        System.out.println("\n-----------------");
        System.out.printf("\u001B[36mTOTAL SIZE:\u001B[0m %dKB\n", totalSize);
        System.out.println("-----------------");
    }

    //DIRECTORY READER CLASS
    static class DirectoryReader implements Callable<Long> {
        //ATTRIBUTE
        private File file;

        //CONSTRUCTOR
        /**
         * DirectoryReader Constructor
         * @param file File to read
         */
        public DirectoryReader(File file) {
            this.file = file;
        }

        //CALL
        @Override
        public Long call() {
            //DIRECTORY
            if (file.isDirectory())
                return getTotalFileSize(file);
            //FILE
            else
                return getFileSize(file);
        }

        //METHODS
        /**
         * This method is used to obtain the file's size.
         * @param f Given File
         * @return file's size in KB (LONG)
         */
        private Long getFileSize(File f) {
            return f.length()/1024;
        }

        /**
         * This method is used when the file is a directory to get the size of all its files.
         * @param f Given Directory
         * @return total file size in KB (LONG)
         */
        private Long getTotalFileSize(File f) {
            Long size = 0L;

            for (File dirFile : Objects.requireNonNull(f.listFiles())) {
                //FILE
                if (dirFile.isFile())
                    size += getFileSize(dirFile);
                //DIRECTORY
                else if (dirFile.isDirectory())
                    size += getTotalFileSize(dirFile);
            }

            return size;
        }
    }
}
