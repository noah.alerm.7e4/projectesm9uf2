package cat.itb.m9.uf2.threads.executors_and_callables.exercise2;

import java.util.concurrent.*;

public class Exercise2 {
    //MAIN
    public static void main(String[] args) {
        //RANDOM NUMBERS
        int numRows = (int) (Math.random() * 20 + 1);
        int numColumns = (int) (Math.random() * 20 + 1);

        //BOARD
        int[][] board = new int[numRows][numColumns];

        //BOARD ASSIGNATION AND PRINTING
        System.out.println("\u001B[35mBOARD:\u001B[0m");
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numColumns; j++) {
                board[i][j] = (int) (Math.random() * 100);
                System.out.printf("%3d", board[i][j]);
            }
            System.out.println();
        }

        //TOTAL COUNTER
        int total = 0;

        //EXECUTOR OPEN
        ExecutorService ex = Executors.newFixedThreadPool(numRows);

        //TOTAL CALCULATION
        System.out.println("\n\u001B[35mROW RESULTS:\u001B[0m");
        for (int i = 0; i < numRows; i++) {
            //CALLABLE
            RowReader r = new RowReader(board[i]);

            //EXECUTOR SUBMIT
            Future<Integer> f = ex.submit(r);

            try {
                //RESULT GETTER
                int rowResult = f.get();

                //TOTAL UPDATE
                total += rowResult;

                //ROW OUTPUT
                System.out.printf("Row %d result: %d\n", i, rowResult);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        //EXECUTOR SHUTDOWN
        ex.shutdown();

        //OUTPUT
        System.out.println("\n-----------------");
        System.out.println("\u001B[36mTOTAL: \u001B[0m" + total);
        System.out.println("-----------------");
    }

    //ROW READER CLASS
    static class RowReader implements Callable<Integer> {
        //ATTRIBUTES
        private int[] row;

        //CONSTRUCTOR
        /**
         * RowReader Constructor
         * @param row Row array
         */
        public RowReader(int[] row) {
            this.row = row;
        }

        //CALL
        @Override
        public Integer call() {
            return readRow();
        }

        //METHODS
        /**
         * This method is used to add all the numbers in a given row (integer array).
         * @return addition result (INT)
         */
        private int readRow() {
            int result = 0;

            for (int num : row) {
                result += num;
            }

            return result;
        }
    }
}
