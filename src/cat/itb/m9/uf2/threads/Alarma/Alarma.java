package cat.itb.m9.uf2.threads.Alarma;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Locale;
import java.util.Scanner;

public class Alarma {
    //ATRIBUTS
    private static Thread ovelles;
    private static Thread alarma;
    private static LocalTime initTime;

    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in).useLocale(Locale.US);
        Locale.setDefault(Locale.US);

        //INPUT
        System.out.print("\u001B[35mQuants segons vols que tardi l'alarma a sonar? (ex. 3): \u001B[0m");
        int segons = sc.nextInt();

        //RUNNABLE
        Runnable r1 = Alarma::comptarOvelles;
        Runnable r2 = () -> alarma(segons);

        //THREADS
        ovelles = new Thread(r1);
        alarma = new Thread(r2);

        //INITIAL TIME
        initTime = LocalTime.now();

        //STARTS
        ovelles.start();
        alarma.start();
    }

    //MÈTODES
    /**
     * Aquest mètode serveix per comptar ovelles.
     */
    public static void comptarOvelles() {
        try {
            int i = 1;

            while (true) {
                //1 ovella
                if (i == 1)
                    System.out.println(i + " ovella");
                //2 o més ovelles
                else
                    System.out.println(i + " ovelles");

                i++;
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Hora de despertar-se...");
        }
    }

    /**
     * Aquest mètode serveix per aturar les ovelles.
     * @param segons Segons que tarda l'alarma a sonar.
     */
    public static void alarma(int segons) {
        String temps = String.format("PT%dS", segons);

        while (!alarma.isInterrupted()) {
            //>= 0 perquè la primera duració ha de ser més gran que la segona.
            if (Duration.between(initTime, LocalTime.now()).compareTo(Duration.parse(temps)) >= 0) {
                ovelles.interrupt();
                alarma.interrupt();
            }
        }
    }
}
