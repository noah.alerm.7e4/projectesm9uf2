package cat.itb.m9.uf2.threads.DeixarDeFumar;

public class DeixarDeFumar {
    //ATRIBUTS
    private static Thread t1, t2;

    //MAIN
    public static void main(String[] args) {
        //RUNNABLE
        Runnable r1 = DeixarDeFumar::fumador;
        Runnable r2 = DeixarDeFumar::metge;

        //THREADS
        t1 = new Thread(r1);
        t2 = new Thread(r2);

        //STARTS
        t1.start();
        t2.start();
    }

    //MÈTODES
    /**
     * Aquest mètode compta el número de cigarretes fumades.
     */
    public static void fumador() {
        int i = 1;
        while (!t1.isInterrupted()) {
            System.out.println("Fumo cigarreta..." + i);
            i++;
        }
    }

    /**
     * Aquest mètode interromp el thread del fumador.
     */
    public static void metge() {
        try {
            Thread.sleep(3000);
            t1.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
